package com.ethickfox.timecounterapp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class ProcessItem  implements Serializable {
    static ArrayList<ProcessItem> itemsList;
    static ArrayList<String> itemsNames;
    private String name;
    private Integer totalMinutes;
    private Long doneMinutes;
    private boolean isGoing;
    private Date startTime;

    static {
        itemsList = new ArrayList<>();
        itemsNames = new ArrayList<>();
    }

    {
        isGoing=false;
        doneMinutes=0L;
    }

    private ProcessItem(String name, Integer totalMinutes){
        this.name = name;
        this.totalMinutes = totalMinutes;
    }

    @Override
    public String toString() {
        return "ProcessItem{" +
                "name="+name+
                "totalMinutes=" +totalMinutes+
                "doneMinutes="+doneMinutes+
                "isGoing="+isGoing+
                "startTime="+startTime+
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    Integer getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(Integer totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

    Long getDoneMinutes() {
        return doneMinutes;
    }

    void setDoneMinutes(Long doneMinutes) {
        this.doneMinutes = doneMinutes;
    }

    boolean isGoing() {
        return isGoing;
    }

    void setGoing(boolean going) {
        isGoing = going;
    }

    Date getStartTime() {
        return startTime;
    }

    void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    static void addItem(String name, Integer totalMinutes){ itemsList.add(new ProcessItem(name,totalMinutes)); itemsNames.add(name); }

    static void addItem(ProcessItem item){ itemsList.add(item); itemsNames.add(item.name); }

    static void removeItem(ProcessItem item){ itemsNames.remove(item.name);itemsList.remove(item);  }
}
