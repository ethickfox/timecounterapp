package com.ethickfox.timecounterapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;

public class AddItemActivity extends AppCompatActivity {

    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        intent = getIntent();
        final MainActivity activity = MainActivity.getInstance();
        if((findViewById(R.id.itemNameField)).requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }


        Button startTimerButton = findViewById(R.id.addButton);

        startTimerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = ((EditText)findViewById(R.id.itemNameField)).getText().toString();
                int time = Integer.parseInt(((EditText)findViewById(R.id.itemTimeField)).getText().toString());

                if(!name.isEmpty() && time!=0){
                    ProcessItem.addItem(name,time);
                    MainActivity.lsadapter.notifyDataSetChanged();
                    try {
                        activity.saveData();
                    } catch (IOException e) {
                        Log.e("APP", "Exception", e);
                    }
                }

                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
