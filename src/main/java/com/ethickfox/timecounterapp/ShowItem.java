package com.ethickfox.timecounterapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ShowItem extends AppCompatActivity {
    String timerState;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_item);
        intent = getIntent();
        final MainActivity activity = MainActivity.getInstance();

        int itemId = getIntent().getIntExtra("ITEM_ID",0);
        final ProcessItem currentItem = ProcessItem.itemsList.get(itemId);

        ((TextView)findViewById(R.id.itemNameView)).setText(currentItem.getName());
        ((TextView)findViewById(R.id.doneTimeView)).setText(String.format(Locale.ENGLISH,
                "%d/%d min.",currentItem.getDoneMinutes(),currentItem.getTotalMinutes()));
        ((ProgressBar)findViewById(R.id.donePB)).setMax(currentItem.getTotalMinutes());
        ((ProgressBar)findViewById(R.id.donePB)).setProgress(currentItem.getDoneMinutes().intValue());


        final Button startTimerButton = findViewById(R.id.startOrStopButton);
        final Button removeButton = findViewById(R.id.removeButton);
        startTimerButton.setText(currentItem.isGoing()?"Stop":"Start");

        startTimerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentItem.isGoing()){
                    long diff = Calendar.getInstance().getTime().getTime() - currentItem.getStartTime().getTime();
                    currentItem.setGoing(false);
                    currentItem.setDoneMinutes(currentItem.getDoneMinutes()+ diff/(60000) );
                }else {
                    currentItem.setGoing(true);
                    currentItem.setStartTime(Calendar.getInstance().getTime());
                }
                try {
                    activity.saveData();
                } catch (IOException e) {
                    Log.e("APP", "Exception", e);
                }
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProcessItem.removeItem(currentItem);
                try {
                    activity.saveData();
                } catch (IOException e) {
                    Log.e("APP", "Exception", e);
                }
                MainActivity.lsadapter.notifyDataSetChanged();
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
