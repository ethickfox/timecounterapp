package com.ethickfox.timecounterapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {
    static ArrayAdapter <String> lsadapter;
    static String fileName;
    static MainActivity instance;
    static {
        fileName = "save.out";
//        ProcessItem.addItem("Test",2000);
    }

    private void readSavedData() throws IOException, ClassNotFoundException, ClassCastException {

        FileInputStream fis = openFileInput(fileName);
        ObjectInputStream is = new ObjectInputStream(fis);
        try {
            while (true){
                ProcessItem.addItem((ProcessItem) is.readObject());
            }
        } catch (EOFException e) {
            // End of stream
        }
        is.close();
        fis.close();
        lsadapter.notifyDataSetChanged();
    }
    void saveData() throws IOException {
        FileOutputStream fos = openFileOutput(fileName, Context.MODE_PRIVATE);
        ObjectOutputStream os = new ObjectOutputStream(fos);
        for (ProcessItem itm : ProcessItem.itemsList)
            os.writeObject(itm);
        os.close();
        fos.close();
    }

    static MainActivity getInstance(){
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FloatingActionButton fab = findViewById(R.id.fab);
        ListView listItemsWidget = findViewById(R.id.listItemsWidget);
        instance = this;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddItemActivity.class);
                startActivity(intent);
            }
        });
        lsadapter = new ArrayAdapter<>(this, R.layout.list_layout, ProcessItem.itemsNames);
        listItemsWidget.setAdapter(lsadapter);
        listItemsWidget.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, ShowItem.class);
                intent.putExtra("ITEM_ID", position);
                startActivity(intent);

            }
        });
        try {
            readSavedData();
        } catch (IOException e) {
            Log.e("APP", "Exception", e);
        } catch (ClassNotFoundException e) {
            Log.e("APP", "Exception", e);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
